<?php

namespace Drupal\auphonic;

use Drupal\auphonic\Form\AuphonicConfigForm;
use Drupal\Core\Config\ConfigFactory;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\Client;

/**
 * Auphonic API creator.
 */
class AuphonicApi {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * Username.
   */
  private string $username;

  /**
   * Password.
   */
  private string $password;

  /**
   * The base host.
   */
  private string $baseHost = 'https://auphonic.com/api/';

  /**
   * Constructs a new Auphonic object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory, KeyRepositoryInterface $keyRepository) {
    $this->client = $client;
    $this->username = $configFactory->get(AuphonicConfigForm::CONFIG_NAME)->get('username') ?? '';
    $password = $configFactory->get(AuphonicConfigForm::CONFIG_NAME)->get('password') ?? '';
    if ($password) {
      $this->password = $keyRepository->getKey($password)->getKeyValue();
    }
  }

  /**
   * Set the username.
   *
   * @param string $username
   *   The username.
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * Set the password.
   *
   * @param string $password
   *   The password.
   */
  public function setPassword($password) {
    $this->password = $password;
  }

  /**
   * Run a file against the simple production API.
   *
   * @param File $file
   *   The audio file.
   * @param array $options
   *   The optional options.
   *
   * @return array
   *   The response.
   */
  public function startSimpleProduction($file, $options = []) {
    // Upload file.
    $guzzleOptions['multipart'] = [
      [
        'name' => 'input_file',
        'contents' => fopen($file->getFileUri(), 'r'),
        'filename' => $file->getFilename(),
      ],
    ];
    // Default value.
    $options['title'] = $options['title'] ?? $file->getFilename();
    $options['action'] = 'start';
    // Add extra options.
    foreach ($options as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $subValue) {
          $guzzleOptions['multipart'][] = [
            'name' => $key,
            'contents' => $subValue,
          ];
        }
      } else {
        $guzzleOptions['multipart'][] = [
          'name' => $key,
          'contents' => $value,
        ];
      }
    }
    $result = json_decode($this->makeRequest("simple/productions.json", [], 'POST', NULL, $guzzleOptions)->getBody(), TRUE);

    return $result;
  }

  /**
   * Run a file against the normal production API.
   *
   * @param File $file
   *   The audio file.
   * @param string $preset
   *   The preset.
   * @param string $title
   *   The title.
   * @param array $options
   *   The optional options.
   *
   * @return array
   *   The response.
   */
  public function startProduction($file, $preset, $title, $options = []) {
    // Create the production.
    $options['preset'] = $preset;
    $options['metadata']['title'] = $title;

    $production = json_decode($this->makeRequest("productions.json", [], 'POST', $options)->getBody(), TRUE);

    if (!isset($production['data']['uuid'])) {
      throw new \Exception('No production uuid found.');
    }
    // Upload file.
    $guzzleOptions['multipart'] = [
      [
        'name' => 'input_file',
        'contents' => fopen($file->getFileUri(), 'r'),
        'filename' => $file->getFilename(),
      ],
    ];
    $id = $production['data']['uuid'];

    $result = json_decode($this->makeRequest("production/$id/upload.json", [], 'POST', NULL, $guzzleOptions)->getBody(), TRUE);
    if (!isset($result['status_code']) || $result['status_code'] != 200) {
      throw new \Exception('Upload to Auphonic failed.');
    }

    $start = json_decode($this->makeRequest("production/$id/start.json", [], 'POST')->getBody(), TRUE);
    return $start;
  }

  /**
   * Get a production.
   *
   * @param string $uuid
   *   The production uuid.
   *
   * @return array
   *   The response.
   */
  public function getProduction($uuid) {
    return json_decode($this->makeRequest("production/{$uuid}.json")->getBody(), TRUE);
  }

  /**
   * Download a production.
   *
   * @param string $uuid
   *   The production uuid.
   * @param string $destination
   *   The destination if piping to a file.
   *
   * @return object
   *   The response object.
   */
  public function downloadProductionFromUuid($uuid, $destination = '') {
    $response = $this->getProduction($uuid);
    if (!isset($response['data']['output_files'][0]['download_url'])) {
      throw new \Exception('No download url found.');
    }
    $response = $this->downloadProduction($response['data']['output_files'][0]['download_url'], $destination);

    return $response;
  }

  /**
   * Download a production from url.
   *
   * @param string $url
   *   The production url.
   * @param string $destination
   *   The destination if piping to a file.
   *
   * @return object
   *   The response object.
   */
  public function downloadProduction($url, $destination = '') {
    // Strip the base path from the url, if its there.
    $url = str_replace($this->baseHost, '', $url);
    $guzzleOptions = [];
    if ($destination) {
      $guzzleOptions['sink'] = $destination;
    }
    $response = $this->makeRequest($url, [], 'GET', NULL, $guzzleOptions);

    return $response;
  }

  /**
   * Get presets.
   *
   * @return array
   *   The response.
   */
  public function getPresets() {
    return json_decode($this->makeRequest("presets.json")->getBody(), TRUE);
  }

  /**
   * Make auphonic call.
   *
   * @param string $path
   *   The path.
   * @param array $query_string
   *   The query string.
   * @param string $method
   *   The method.
   * @param array $body
   *   Data to attach if POST/PUT/PATCH.
   * @param array $options
   *   Extra headers.
   *
   * @return string|object
   *   The return response.
   */
  protected function makeRequest($path, array $query_string = [], $method = 'GET', $body = '', array $options = []) {
    if (!$this->username || !$this->password) {
      throw new \Exception('No username or password set.');
    }
    // Don't wait to long.
    $options['connect_timeout'] = 30;
    $options['read_timeout'] = 30;
    $options['timeout'] = 30;

    // Credentials
    $options['auth'] = [$this->username, $this->password];
    if ($body) {
      $options['headers']['content-type'] = 'application/json';
      $options['body'] = json_encode($body);
    }

    $new_url = rtrim($this->baseHost, '/') . '/' . $path;
    $new_url .= count($query_string) ? '?' . http_build_query($query_string) : '';

    $res = $this->client->request($method, $new_url, $options);

    return $res;
  }

}
