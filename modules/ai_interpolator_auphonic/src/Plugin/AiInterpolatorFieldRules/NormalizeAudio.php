<?php

namespace Drupal\ai_interpolator_auphonic\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\auphonic\AuphonicApi;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\Entity\File;
use Drupal\file\FileRepositoryInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for an audio field
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_auphonic_normalize_audio",
 *   title = @Translation("Auphonic Normalize Audio"),
 *   field_rule = "file",
 *   target = "file",
 * )
 */
class NormalizeAudio extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Auphonic Normalize Audio';

  /**
   * The Auphonic API.
   */
  public AuphonicApi $auphonicApi;

  /**
   * The token.
   */
  public Token $token;

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file system.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The current user.
   */
  public AccountInterface $currentUser;

  /**
   * The guzzle client.
   */
  public Client $guzzleClient;

  /**
   * The constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AuphonicApi $auphonicApi, Token $token, EntityTypeManagerInterface $entityTypeManager, FileSystemInterface $fileSystem, AccountInterface $user, Client $guzzleClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->auphonicApi = $auphonicApi;
    $this->token = $token;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->currentUser = $user;
    $this->guzzleClient = $guzzleClient;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('auphonic.api'),
      $container->get('token'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('current_user'),
      $container->get('http_client')
    );
  }


  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function tokens() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return [
      'file',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    $tries = 10;
    foreach ($entity->{$interpolatorConfig['base_field']} as $entityWrapper) {
      if ($entityWrapper->entity) {
        $fileEntity = $entityWrapper->entity;
        $results = $this->auphonicApi->startProduction($fileEntity);
        // If it started a production, we start polling every 5 seconds.
        if (isset($results['data']['uuid'])) {
          while ($results['data']['status'] != 3 && $tries > 0) {
            sleep(5);
            $results = $this->auphonicApi->getProduction($results['data']['uuid']);
            $tries--;
          }
        }
        if (isset($results['data']['output_files'][0]['download_url'])) {
          $values[] = [
            'uri' => $results['data']['output_files'][0]['download_url'],
            'title' => $results['data']['output_files'][0]['filename'],
          ];
        }
      }

    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Check so the value is a valid url.
    if (isset($value['uri']) && filter_var($value['uri'], FILTER_VALIDATE_URL)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Download file and generate a file entity from the download url.
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/'));
    $fileStorage = $this->entityTypeManager->getStorage('file');
    // Prepare directory.
    $this->fileSystem->prepareDirectory($filePath, FileSystemInterface::CREATE_DIRECTORY);
    $fileEntities = [];
    foreach ($values as $value) {
      $destination = $filePath . '/' . str_replace('.mp3', '.auphonic.mp3', $value['title']);
      $response = $this->auphonicApi->downloadProduction($value['uri'], $destination);
      if ($response->getStatusCode() == 200) {
        $file = $fileStorage->create([
          'uri' => $destination,
          'filename' => $value['title'],
          'filemime' => mime_content_type($destination),
          'uid' => $this->currentUser->id(),
          'status' => File::STATUS_PERMANENT,
        ]);
        $file->save();
        $fileEntities[] = $file;
      }
    }
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

}
