<?php

namespace Drupal\auphonic\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure auphonic API access.
 */
class AuphonicConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'provider_auphonic.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auphonic_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auphonic Username'),
      '#description' => $this->t('The username. Can be found on <a href="https://auphonic.com/accounts/settings/">https://auphonic.com/accounts/settings/</a>.'),
      '#default_value' => $config->get('username'),
    ];

    $form['password'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Auphonic Password'),
      '#description' => $this->t('The username. Can be found on <a href="https://auphonic.com/accounts/settings/">https://auphonic.com/accounts/settings/</a>.'),
      '#default_value' => $config->get('password'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
