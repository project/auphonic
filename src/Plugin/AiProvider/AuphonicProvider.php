<?php

namespace Drupal\auphonic\Plugin\AiProvider;

use Drupal\ai\Attribute\AiProvider;
use Drupal\ai\Base\AiProviderClientBase;
use Drupal\ai\Exception\AiBadRequestException;
use Drupal\ai\OperationType\AudioToAudio\AudioToAudioInput;
use Drupal\ai\OperationType\AudioToAudio\AudioToAudioInterface;
use Drupal\ai\OperationType\AudioToAudio\AudioToAudioOutput;
use Drupal\ai\OperationType\GenericType\AudioFile;
use Drupal\auphonic\AuphonicApi;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\File\FileExists;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Plugin implementation of the 'auphonic' provider.
 */
#[AiProvider(
  id: 'auphonic',
  label: new TranslatableMarkup('Auphonic'),
)]
class AuphonicProvider extends AiProviderClientBase implements
  ContainerFactoryPluginInterface,
  AudioToAudioInterface {

  /**
   * The Auphonic Client.
   *
   * @var \Drupal\auphonic\AuphonicApi
   */
  protected $auphonicClient;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The temporary files.
   *
   * @var array
   */
  protected $temporaryFiles = [];

  /**
   * Destructor.
   */
  public function __destruct() {
    foreach ($this->temporaryFiles as $file) {
      $file->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->auphonicClient = $container->get('auphonic.api');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredModels(string $operation_type = NULL, array $capabilities = []): array {
    $response = $this->auphonicClient->getPresets();

    $models = [];
    if (isset($response['data'])) {
      foreach  ($response['data'] as $preset) {
        $models[$preset['uuid']] = $preset['preset_name'];
      }
    }
    return $models;
  }

  /**
   * {@inheritdoc}
   */
  public function isUsable(string $operation_type = NULL, array $capabilities = []): bool {
    // If its not configured, it is not usable.
    if (!$this->getConfig()->get('password')) {
      return FALSE;
    }
    // If its one of the bundles that Mistral supports its usable.
    if ($operation_type) {
      return in_array($operation_type, $this->getSupportedOperationTypes());
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedOperationTypes(): array {
    return [
      'audio_to_audio',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): ImmutableConfig {
    return $this->configFactory->get('provider_auphonic.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getApiDefinition(): array {
    // Load the configuration.
    $definition = Yaml::parseFile($this->moduleHandler->getModule('auphonic')->getPath() . '/definitions/api_defaults.yml');
    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getModelSettings(string $model_id, array $generalConfig = []): array {
    return $generalConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function setAuthentication(mixed $authentication): void {
    // Set the new API key and reset the client.
    $this->auphonicClient->setUsername($authentication['username']);
    $this->auphonicClient->setPassword($authentication['password']);
  }

  /**
   * {@inheritdoc}
   */
  public function audioToAudio(string|array|AudioToAudioInput $input, string $model_id, array $tags = []): AudioToAudioOutput {
    // Normalize the input if needed.
    $title = "";
    $options = [];
    $audio_input = "";
    if ($input instanceof AudioToAudioInput) {
      $audio_input = "";
      $audio_input = $this->generateTemporaryFile($input->getAudioFile()->getBinary(), $input->getAudioFile()->getFilename());
      if (empty($this->configuration['title'])) {
        throw new AiBadRequestException('Title is required for Auphonic');
      }
      $title = $this->configuration['title'];
      $options = $this->configuration;
    }
    else {
      // Otherwise extract raw input to parts.
      $audio_input = $this->generateTemporaryFile($input['audio_binary'], 'audio.mp3');
      $title = $input['title'];
      $options = $input['options'] ?? [];
    }
    // Start a production.
    $response = $this->auphonicClient->startProduction($audio_input, $model_id, $title, $options);
    // Wait for production to finish.
    $tries = 20;
    while ($response['data']['status'] != 3 && $tries > 0) {
      sleep(5);
      $response = $this->auphonicClient->getProduction($response['data']['uuid']);
      $tries--;
    }
    if (!isset($response['data']['output_files'][0]['download_url'])) {
      throw new AiBadRequestException('No download url found for Auphonic.');
    }

    $response_object = $this->auphonicClient->downloadProduction($response['data']['output_files'][0]['download_url']);
    $binary = $response_object->getBody()->getContents();

    // Create an output file.
    $output = new AudioFile($binary, 'audio/' . $response['data']['output_files'][0]['format'], $response['data']['output_files'][0]['filename']);
    return new AudioToAudioOutput([$output], $response['filename'], $response['data']['metadata']);
  }

  /**
   * Generate a temporary file.
   *
   * @param string $binary
   *   The binary.
   * @param string $filename
   *   The filename.
   *
   * @return \Drupal\file\Entity\File
   *   The file.
   */
  protected function generateTemporaryFile(string $binary, string $filename): File {
    $tmp_path = $this->fileSystem->getTempDirectory() . '/' . $filename;
    $path = $this->fileSystem->saveData($binary, $tmp_path, FileExists::Replace);
    $fileStorage = $this->entityTypeManager->getStorage('file');
    $file = $fileStorage->create([
      'uri' => $path,
      'filename' => $filename,
      'status' => 0,
    ]);
    $file->save();
    // Set the file for desctruction when finished.
    $this->temporaryFiles[] = $file;
    return $file;
  }


  /**
   * Gets the raw client.
   *
   * This is the client for inference.
   *
   * @return \Drupal\auphonic\AuphonicApi
   *   The Auphonic API.
   */
  public function getClient(): AuphonicApi {
    return $this->auphonicClient;
  }

}
